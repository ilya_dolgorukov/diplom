#!/usr/bin/env python3
from pwn import ELF
import angr
import argparse
import type_expl
import choice_expl
import final_expl
import r2pipe
import json

stdin = "STDIN"
arg = "ARG"

def get_system(name_file):

    system_func = {}
    r2 = r2pipe.open(name_file)
    r2.cmd("aac")

    functions = [func for func in json.loads(r2.cmd("aflj"))]
    for func in functions:
        if "system" in str(func["name"]):
            system_name = func["name"]
            refs = [ func for func in json.loads(r2.cmd("axtj @ {}".format(system_name))) ]
            for ref in refs:
                if "fcn_name" in ref:
                    system_func[ref["fcn_name"]] = ref

    return system_func

def mitigations(name_file):

    prop = {}
    binary = ELF(name_file, checksec=False)
    prop["aslr"] = binary.aslr
    prop["arch"] = binary.arch
    prop["canary"] = binary.canary
    prop["got"] = binary.got
    prop["nx"] = binary.nx
    prop["pie"] = binary.pie
    prop["plt"] = binary.plt
    prop["relro"] = binary.relro
    return prop

def inp_type(name_file):

    p = angr.Project(name_file, load_options={"auto_load_libs": False})
    reading_functions = ["fgets", "gets", "scanf", "read", "__isoc99_scanf"]
    binary_functions = list(p.loader.main_object.imports.keys())

    if any([x in reading_functions for x in binary_functions]):
        return "STDIN"
    return "ARG"

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("file")

    args = parser.parse_args()
    if args.file is None:
        print("[-] file not found")
        exit(1)

    prop = {}
    prop["input_type"] = inp_type(args.file)
    prop["file"] = args.file
    prop["pwn_type"] = {}
    prop["pwn_type"]["type"] = None
    prop["sys_func"] = []
    prop["dlresolve"] = True
    prop["sys_func"] = get_system(args.file)
    prop["pwn_type"] = type_expl.overflow_type( args.file, inputType=prop["input_type"] )
    prop["protections"] = mitigations(args.file)

    if prop["pwn_type"]["type"] == "Overflow":

        prop["pwn_type"]["results"] = {}
        prop["pwn_type"]["results"] = choice_expl.exploit_f( args.file, prop, inputType=prop["input_type"] )
        final_expl.explotation( args.file, prop )

    elif prop["pwn_type"]["type"] == "overflow_variable":
        prop["pwn_type"]["results"] = prop["pwn_type"]
        final_expl.explotation( args.file, prop )

    else:
        print("file is not pwnable")
    

if __name__ == "__main__":
    main()