import angr
import claripy
from manager_helper import detect_overflow, hook_win

def overflow_type(name_file, inputType="STDIN"):

    p = angr.Project(name_file, load_options={"auto_load_libs": False})

    p.hook_symbol("system", hook_win())

    argv = [name_file]
    input = claripy.BVS("input", 300 * 8)
    if inputType == "STDIN":
        state = p.factory.full_init_state(args=argv, stdin=input)
        state.globals["user_input"] = input
    else:
        argv.append(input)
        state = p.factory.full_init_state(args=argv)
        state.globals["user_input"] = input

    state.libc.buf_symbolic_bytes = 0x100
    state.globals["inputType"] = inputType
    manager = p.factory.simgr( state, save_unconstrained=True )

    cur_env = {}
    cur_env["type"] = None
    end_state = None
    try:
        def expl_bin(manager):
            manager.explore( find=lambda s: "type" in s.globals, step_func=detect_overflow)

        expl_bin(manager)
        if "found" in manager.stashes and len(manager.found):
            end_state = manager.found[0]
            cur_env["type"] = end_state.globals["type"]

    except (KeyboardInterrupt) as e:
        print("time is expired")

    if "input" in cur_env.keys() or cur_env["type"] == "overflow_variable":
        cur_env["input"] = end_state.globals["input"]
    
    return cur_env
