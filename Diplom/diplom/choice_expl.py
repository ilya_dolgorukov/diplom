from angr import sim_options as so
from manager_helper import *
import claripy
import os
import r2pipe
import json

def getRegValues(filename, endAddr=None):

    r2 = r2pipe.open(filename, flags=["-d"])
    if endAddr:
        r2.cmd("dcu {}".format(endAddr))
    else:
        r2.cmd("e dbg.bep=entry")
        entry_addr = json.loads(r2.cmd("iej"))[0]["vaddr"]
        r2.cmd("dcu {}".format(entry_addr))

    regs = json.loads(r2.cmd("drrj"))
    regs = dict([(x["reg"], int(x["value"], 16)) for x in regs if x["reg"] != "rflags" and x["reg"] != "eflags"])
    r2.quit()
    return regs

def get_base_addr(filename):

    r2 = r2pipe.open(filename)
    r2.cmd("doo")
    base_addr = json.loads(r2.cmd("iMj"))["vaddr"]
    r2.quit()
    return base_addr

def exploit_f(binary_name, prop, inputType="STDIN"):

    cur_env = prop["pwn_type"].get("results", {})
    cur_env["type"] = cur_env.get("type", None)

    p = angr.Project(binary_name, load_options={"auto_load_libs": False})
    if prop.get("libc", None) and not isinstance(prop["libc"], dict):
        libc_base_addr = prop.get("libc_base_address", 0x500000)
        libc_base_name = os.path.basename(prop["libc"])
        p = angr.Project(
            binary_name,
            load_options={"auto_load_libs": False},
            force_load_libs=[prop["libc"]],
            lib_opts={libc_base_name: {"base_addr": libc_base_addr}} )
        
    if p.loader.main_object.pic:
        base_addr = get_base_addr(binary_name)
        p = angr.Project(
            binary_name,
            load_options={
                "auto_load_libs": False,
                "main_opts": {"base_addr": base_addr}} )
    extras = {so.REVERSE_MEMORY_NAME_MAP}

    has_pie = prop.get("protections", {}).get("pie", False)

    argv = [binary_name]
    input_arg = claripy.BVS("input", 400 * 8)
    if inputType == "STDIN":
        entry_addr = p.loader.main_object.entry
        if not has_pie:
            reg_values = getRegValues(binary_name, entry_addr)
        state = p.factory.full_init_state( args=argv, add_options=extras, stdin=input_arg, env=os.environ )

        if not has_pie:
            register_names = list(state.arch.register_names.values())
            for register in register_names:
                if register in reg_values:  
                    state.registers.store(register, reg_values[register])
    else:
        argv.append(input_arg)
        state = p.factory.full_init_state(args=argv, add_options=extras)

    state.globals["needs_leak"] = True
    if cur_env["type"] == "leak":
        state.globals["needs_leak"] = False
        state.globals["leak_input"] = cur_env["leak_input"]
        for x, y in enumerate(cur_env["leak_input"]):
            state.add_constraints(input_arg.get_byte(x) == y)

    state.libc.buf_symbolic_bytes = 0x100
    state.globals["user_input"] = input_arg
    state.globals["inputType"] = inputType
    state.globals["prop"] = prop
    manager = p.factory.simgr(state, save_unconstrained=True)

    step_func = run_exploit(prop)
    if step_func is None:
        exit(1)

    manager.explore(find=lambda s: "type" in s.globals, step_func=step_func)
    try:
        def expl_bin(manager):
            manager.explore(find=lambda s: "type" in s.globals, step_func=step_func)

        expl_bin(manager)

    except (KeyboardInterrupt) as e:
        return cur_env
    end_state = manager.found[0]
    cur_env["type"] = end_state.globals["type"]
    if cur_env["type"] == "leak":
        cur_env["leak_input"] = end_state.globals["leak_input"] + b"\n"
        cur_env["leak_output"] = end_state.globals["output_before_leak"]
        cur_env["leaked_function"] = end_state.globals["leaked_func"]

    if cur_env["type"] == "dlresolve":
        cur_env["dlresolve_first"] = end_state.globals["dlresolve_first"]
        cur_env["dlresolve_second"] = end_state.globals["dlresolve_second"]

    cur_env["input"] = end_state.globals.get("input", None)
    return cur_env

    
def run_exploit(prop):

    has_nx = prop.get("protections", {}).get("nx", True)
    if prop.get("sys_func", None):
        return ret_to_system
    
    elif not has_nx: 
        return ret_to_shellcode

    else:
        return ret_to_rop